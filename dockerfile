FROM node:alpine

ARG BUILD_DATE
ARG VCS_REF

LABEL MAINTAINER="max@maxtpower.com" \
org.label-schema.schema-version="1.0" \
org.label-schema.build-date=${BUILD_DATE} \
org.label-schema.name="maxtpower/fserve" \
org.label-schema.description="HTTP File Transfer Service" \
org.label-schema.vcs-url="https://gitlab.com/m1xp3r/fserve" \
org.label-schema.vcs-ref=${VCS_REF} \
org.label-schema.vendor="maxtpower"

WORKDIR /www

RUN npm install --global http-server

ENTRYPOINT [ "http-server"]
CMD [ "/www", "-p 80", "-c -1"]

EXPOSE 80
